<?php

namespace justjob\controleurs;

use \justjob\models\Candidature;
use \justjob\models\Categorie;

class ControleurCandidature extends Controleur{
	
	
	public function choisirCandidature($request, $response, $args){
		$idCand = intval($args['cand']);

		if(!Auth::estConnecte()){
			Flash::flash("erreur", "Vous devez être connecté pour effectuer cette action");
			return Utils::redirect($response, "formulaireConnexion");
		}

		$cand = Candidature::find($idCand);
		if($cand == null){
			throw new \Slim\Exception\NotFoundException($request, $response);
		}

		$ref = Candidature::where("id_offre", "=", $cand->id_offre)->get();

		foreach ($ref as $r) {
			$r->etat = "refusee";
			$r->save();
		}

		$cand->etat = "acceptee";
		$cand->save();


		$offre = $cand->offre;
		$offre->etat = false;
		$offre->save();
		

		return Utils::redirect($response, "afficherMonOffre", ['id' => $cand->id_offre]);
	}
}