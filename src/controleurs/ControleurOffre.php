<?php

namespace justjob\controleurs;
use justjob\models\Categorie as Categorie;
use justjob\models\User as User;
use justjob\models\Candidature as Candidature;

class ControleurOffre extends Controleur{
	
	public function afficherOffres($request, $response, $args){

		$categorie = Categorie::orderBy('id', 'DESC')->get();

		//$categorie = Categorie::select('profil')->orderBy('id', 'DESC')->get();
		//$categorie = Categorie::select('*')->get();
		$tabCate;
		foreach($categorie as $categorie){
			$tabCate[]=$categorie;
	}
		//var_dump($tabCate);
		return $this->view->render($response,"Offre.html",compact("tabCate"));
	}
	
	/**
	public function afficherAccueil($request, $response, $args){
		return $this->view->render($response, "base.html",compact($nomvar);
	}
	*/

 public function afficherAjouterOffre($request, $response, $args)
 {
 	return $this->view->render($response,"ajouterOffre.html");
 }

 public function ajouterOffre($request, $response, $args)
 {
 	$profil = Utils::getFilteredPost($request, "profil");
 	$duree = Utils::getFilteredPost($request, "duree");
	$lieu = Utils::getFilteredPost($request, "lieu");
	$etat = Utils::getFilteredPost($request, "choixEtat");
	$activite = Utils::getFilteredPost($request, "activite");
	$user_id = Auth::getIdUser();

	if($user_id == null)
	{
		Flash::flash("erreur", "L'utilisateur doit être connecté");
		return Utils::redirect($response, "afficherFormulaireOffre");
	}

	if($profil == null || $lieu == null)
	{
		Flash::flash("erreur", "L'utilisateur doit être connecté");
		return Utils::redirect($response, "afficherFormulaireOffre");
	}

	$offre = new Categorie();
	$offre->profil = $profil;
	$offre->duree = $duree;
	$offre->lieu = $lieu;
	$offre->activite = $activite;
	$offre->etat = true;
	$offre->id_user = $user_id;
	$offre->save();
	return Utils::redirect($response, "afficherMesOffres");
 }
 
 public function afficherOffreDetail($request, $response, $args){
	 $offre = $args["num"];
	 $categorie = Categorie::select('*')->where("id","like",$offre)->get();
		$tabCate;
		foreach($categorie as $categorie){
		$tabCate[]=$categorie;
	}
	$tabEmploy=null;
	if(isset($tabCate[0]->id_user)){
		//var_dump($tabCate[0]->id_user);
		$employeur= User::select('*')->where("id","like",$tabCate[0]->id_user)->get(); 
		foreach($employeur as $employeur){
			$tabEmploy[]=$employeur;
		}
		
		
	}
	 return $this->view->render($response,"OffreDetail.html",compact("tabCate","tabEmploy"));
 }

 public function afficherPostuler($request, $response, $args)
 {
 	$idOffre = $args['num'];
 	$offre = Categorie::find($idOffre);
 	return $this->view->render($response,"postuler.html", compact('offre'));
 }

 public function ajouterPostuler($request, $response, $args)
 {
 	$idOffre = intval($args['num']);
	$offre = Categorie::find($idOffre);

 	$lieu = Utils::getFilteredPost($request, "lieu");
 	$vehicule = Utils::getFilteredPost($request, "vehicule");

 	$idCandidat = Auth::getIdUser();

 	$candidature = new Candidature();
 	$candidature->id_offre = $idOffre;
 	$candidature->id_candidat = $idCandidat;
 	$candidature->etat = "en attente";
 	$candidature->depart = $lieu;
 	$candidature->transport = $vehicule;
 	$candidature->save();

 	return Utils::redirect($response, "AffichOffres");

 }

 public function rechercher($request, $response, $args)
 {
 	return $this->view->render($response,"rechercher.html");
 }

}