<?php

namespace justjob\controleurs;

use justjob\models\User;

class Auth
{

	 private static function startSession()
	 {
            if (session_status() != PHP_SESSION_ACTIVE)
                session_start();
	 }
        
	private static function verifierMdp(string $email, string $mdp){
        $user = User::where("email", "=", $email)->first();
        return ($user != null && password_verify($mdp, $user->mdp)) ? $user : null;
	}

	public static function estConnecte()
    {
            static::startSession();
            return isset($_SESSION['user']);
    }

    public static function deconnexion(){
    	static::startSession();
    	unset($_SESSION['user']);
    }

    public static function inscription(string $email, string $nom, string $prenom, string $mdp)
    {
            static::startSession();

            $u = new User();
            $u->email = $email;
            $u->nom = $nom;
            $u->prenom = $prenom;
            $u->mdp = password_hash($mdp, PASSWORD_DEFAULT);

            if(!$u->save()){ return -1;}

            $_SESSION['user'] = $user->id;
            return $u->id;
    }

	public static function connexion(string $email, string $mdp){
		 if(static::estConnecte())
                return true;

        static::startSession();

        $user = static::verifierMdp($email, $mdp);
        if($user)
        {
                $_SESSION['user'] = $user->id;
                return true;
        }
        return false;
	}

    public static function getIdUser() : int
        {
            static::startSession();
            if (!static::estConnecte())
                return -1;

            return intval($_SESSION['user']);
        }


}