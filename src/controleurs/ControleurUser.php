<?php

namespace justjob\controleurs;

use \justjob\models\User;
use \justjob\controleurs\Auth;
use \justjob\models\Categorie;
use \justjob\models\Candidature;


class ControleurUser extends Controleur
{

    public function afficherFormulaireInscription($request, $response, $args)
    {
    	if(Auth::estConnecte()){
			return Utils::redirect($response, "accueil");
		}
		return $this->view->render($response, "formulaireInscription.html");
    }

    public function inscrire($request, $response, $args)
    {

    	$email = Utils::getFilteredPost($request, "email");
        $nom = Utils::getFilteredPost($request, "nom");
        $prenom = Utils::getFilteredPost($request, "prenom");
        $mdp = $request->getParsedBodyParam("mdp", null);
        $mdpConf = $request->getParsedBodyParam("confirmMdp", null);

        if ($email == null || $nom == null || $prenom == null || $mdp === null || $mdpConf == null) {
        	Flash::flash("erreur", "Tous les champs doivent être remplis");
            return Utils::redirect($response, "formulaireInscription");
        }
        if (User::where("email", "=", $email)->count()){
        	Flash::flash("erreur", "Le nom est déja présent dans la base");
            return Utils::redirect($response, "formulaireInscription");
        }
        if ($mdp !== $mdpConf){
        	Flash::flash("erreur", "Le mots de passe ne correspondent pas");
        	return Utils::redirect($response, "formulaireInscription");
     	}

        $id = Auth::inscription($email, $nom, $prenom, $mdp);
        if ($id === -1)
        {
            Flash::flash("erreur", "Impossible de créer un compte");
            return Utils::redirect($response, "formulaireInscription");
        }
        else
        {  
            return Utils::redirect($response, "formulaireConnexion");
        }
    }


	public function connecter($request, $response, $args){
		$email = Utils::getFilteredPost($request, "email");
		$mdp = $request->getParsedBodyParam("mdp");

		if($email === null || $mdp === null){
			Flash::flash("erreur", "Veuillez renseigner vos informations de connexion");
            return Utils::redirect($response, "formulaireConnexion");
		}

		$res = Auth::connexion($email, $mdp);
        if (!$res)
        {
           	Flash::flash("erreur", "Nom d'utilisateur ou mot de passe incorrect !");
            return Utils::redirect($response, "formulaireConnexion");
        }

        return Utils::redirect($response, "accueil");
	}

	public function afficherFormulaireConnexion($request, $response, $args)
    {
    	if(Auth::estConnecte()){
			return Utils::redirect($response, "accueil");
		}

        return $this->view->render($response, "formulaireConnexion.html");
    }

    public function deconnecter($request, $response, $args){
    	Auth::deconnexion();
    	return Utils::redirect($response, "accueil");
    }

    public function afficherCompte($request, $response, $args){
        if(!Auth::estConnecte()){
            Flash::flash("erreur", "Vous devez être connecté pour pouvoir accéder à votre compte");
            return Utils::redirect($response, "formulaireConnexion");
        }

        return $this->view->render($response, "compte/compte.html");
    }

    public function afficherMesOffres($request, $response, $args){
        if(!Auth::estConnecte()){
            Flash::flash("erreur", "Vous devez être connecté pour pouvoir accéder à votre compte");
            return Utils::redirect($response, "formulaireConnexion");
        }

        $offres = Categorie::where('id_user','=',Auth::getIdUser())->get();

        return $this->view->render($response, "compte/mesOffres.html", compact("offres"));
    }

    public function afficherMonOffre($request, $response, $args){
        $id = intval($args['id']);

        $offre = Categorie::find($id);

        if($offre == null || $offre->id_user !== Auth::getIdUser()){
            throw new \Slim\Exception\NotFoundException($request, $response);
        }

        $candidatures = Candidature::where("id_offre","=",$offre->id)->get();

        return $this->view->render($response, "compte/offreDetails.html", compact("offre", "candidatures"));
    }
}