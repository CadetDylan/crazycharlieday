<?php


use \justjob\Migration\Migration;
use \justjob\models\User;
use Illuminate\Database\Schema\Blueprint;

class UserTableMigration extends Migration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $this->schema->dropIfExists("user");
        $this->schema->create("user", function(Blueprint $table){
            $table->increments('id')->unique();
            $table->string('nom');
             $table->string('img')->default('');;
            $table->timestamps();
        });

        $path_to_root = ".";
        $url = "/$path_to_root/ressources/img/";

        User::create(["nom" => "Cassandro", "img" => $url . "1.jpg"]);
        User::create(["nom" => "Achille", "img" => $url . "2.jpg"]);
        User::create(["nom" => "Calypso", "img" => $url . "3.jpg"]);
        User::create(["nom" => "Bacchus", "img" => $url . "4.jpg"]);
        User::create(["nom" => "Diane", "img" => $url . "5.jpg"]);
        User::create(["nom" => "Clark", "img" => $url . "6.jpg"]);
        User::create(["nom" => "Jason", "img" => $url . "7.jpg"]);
        User::create(["nom" => "Bruce", "img" => $url . "8.jpg"]);

    }
    public function down()
    {
        $this->schema->drop("user");
    }
}
