<?php

namespace justjob\models;

class User extends BaseModel{

	protected $table = 'user';
	protected $primaryKey = 'id';

	public $timestamps = false;

	public function listeOffres()
	{
		return $this->belongsTo(Categorie::class);
	}


	protected function doDelete() :bool {
		return true;
	}

}
