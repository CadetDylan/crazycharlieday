<?php

namespace justjob\models;

Use \Illuminate\Database\Eloquent\Model as Model;

class Candidature extends Model{
  protected $table = 'candidature';
  protected $primaryKey = 'id';
  public $timestamps = false;


  public function candidat(){
  		return $this->belongsTo(User::class, 'id_candidat');
  }

  public function offre(){
  	return $this->belongsTo(Categorie::class, 'id_offre');
  }

}
