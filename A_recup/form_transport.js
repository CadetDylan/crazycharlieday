
window.addEventListener("load",()=>{

  let pc =  document.querySelector("#pc");
  let rc =  document.querySelector("#rc");
  let rtp =  document.querySelector("#rtp");
  let transport_pc =  document.querySelector("#transport_pc");
  let transport_rc =  document.querySelector("#transport_rc");
  let transport_rtp =  document.querySelector("#transport_rtp");

  let pc_bool=false,rc_bool=false,rtp_bool=false

  function toggleTransportMain(obj){
    console.log(obj)
    switch (obj) {
      case pc:
        if(!pc_bool){
          transport_pc.classList.toggle("hidden");
          pc_bool=true;
        }
        if(rc_bool){
          transport_rc.classList.toggle("hidden");
          rc_bool=false;
        }
        if(rtp_bool){
          transport_rtp.classList.toggle("hidden");
          rtp_bool=false;
        }
        break;
      case rc:
        if(pc_bool){
          transport_pc.classList.toggle("hidden");
          pc_bool=false;
        }
        if(!rc_bool){
          transport_rc.classList.toggle("hidden");
          rc_bool=true;
        }
        if(rtp_bool){
          transport_rtp.classList.toggle("hidden");
          rtp_bool=false;
        }
        break;
      case rtp:
        if(pc_bool){
          transport_pc.classList.toggle("hidden");
          pc_bool=false;
        }
        if(rc_bool){
          transport_rc.classList.toggle("hidden");
          rc_bool=false;
        }
        if(!rtp_bool){
          transport_rtp.classList.toggle("hidden");
          rtp_bool=true;
        }
        break;
    }
  }

  pc.addEventListener("click", (event) => {
    toggleTransportMain(pc);
  });
  rc.addEventListener("click", (event) => {
    toggleTransportMain(rc);
  });
  rtp.addEventListener("click", (event) => {
    toggleTransportMain(rtp);
  });
});
