
window.addEventListener("load",()=>{

  let h = false;
  let h_2 = false;


  let handicap_yes = document.querySelector("#handicap_yes");
  let handicap_no = document.querySelector("#handicap_no");
  let handicap_type = document.querySelector("#handicap_type");
  let handicap_ph = document.querySelector("#handicap_ph");
  let handicap_me = document.querySelector("#handicap_me");
  let handicap_physique= document.querySelector("#handicap_physique");
  let handicap_mental = document.querySelector("#handicap_mental");

  function toggleHandicap(obj1, obj2){
    obj1.classList.toggle("hidden");
    if(obj2!=null && !obj2.classList.contains("hidden")){
      obj2.classList.toggle("hidden");
    }
  }

  handicap_yes.addEventListener("click", (event) => {
    if(!h){
      toggleHandicap(handicap_type);
      h=true;
    }
  });

  handicap_no.addEventListener("click", (event) => {
    if(h){
      toggleHandicap(handicap_type);
      h=false;
    }
  });

  handicap_ph.addEventListener("click", (event) => {
    if(!h_2){
      toggleHandicap(handicap_physique, handicap_mental);
      h_2=true;
    }
  });

  handicap_me.addEventListener("click", (event) => {
    if(h_2){
      toggleHandicap(handicap_mental, handicap_physique);
      h_2=false;
    }
  });

});
