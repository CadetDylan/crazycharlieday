-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Mer 06 Février 2019 à 15:38
-- Version du serveur :  5.7.25-0ubuntu0.18.04.2
-- Version de PHP :  7.0.28-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ccd2019`
--

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE `categorie` (
  `id` int(11) NOT NULL,
  `id_user`int(11) NOT NULL,
  `profil` varchar(50) CHARACTER SET utf8 NOT NULL,
  `duree` varchar(50) CHARACTER SET utf8 NOT NULL,
  `lieu` varchar(200) CHARACTER SET utf8 NOT NULL,
  `etat` tinyint(1) NOT NULL,
  `activite` varchar(100) CHARACTER SET utf8 NOT NULL

) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `categorie`
--

INSERT INTO `categorie` (`id`, `profil`, `duree`, `lieu`,`etat`,`activite`) VALUES
(1,'assistant comptable', "3mois", 'Nancy', true, 'Administratif'),
(2, 'comptable',"3mois", 'Nancy', false, 'Administratif'),
(3, 'secrétaire',"3mois", 'Metz', true, 'Administratif'),
(4, 'standardiste',"3mois", 'Nancy', true, 'Administratif'),
(5, 'comptable',"3mois", 'Metz', true, 'Administratif'),
(6, 'secrétaire',"3mois", 'Nancy', false, 'Administratif'),
(7, 'standardiste',"3mois", 'Nancy', true, 'Administratif'),
(8, 'conducteur d\'engin',"3mois", 'Metz', false, 'Bâtiment/Travaux Publics'),
(9, 'manœuvre',"3mois", 'Nancy', true, 'Bâtiment/Travaux Publics'),
(10, 'maçon',"3mois", 'Nancy', true, 'Bâtiment/Travaux Publics'),
(11, 'électricien',"3mois", 'Nancy', true, 'Bâtiment/Travaux Publics'),
(12, 'assistant commercial',"3mois", 'Metz', true, 'Commerce et vente '),
(13, 'commercial',"3mois", 'Nancy', true, 'Commerce et vente '),
(14, 'manager',"3mois", 'Nancy', true, 'Commerce et vente '),
(15, 'vendeur polyvalent',"3mois", 'Nancy', true, 'Commerce et vente '),
(16, 'magasinier',"3mois", 'Nancy', true, 'Logistique'),
(17, 'préparateur de commandes',"3mois", 'Nancy', false, 'Logistique'),
(18, 'aide cuisinier',"3mois", 'Nancy', true, 'Restauration et hôtellerie'),
(19, 'cuisinier',"3mois", 'Nancy', true, 'Restauration et hôtellerie'),
(20, 'employé polyvalent',"3mois", 'Nancy', true, 'Restauration et hôtellerie'),
(21, 'serveur',"3mois", 'Metz', true, 'Restauration et hôtellerie'),
(22, 'cariste',"3mois", 'Nancy', false, 'Transport'),
(23, 'chauffeur de bus',"3mois", 'Nancy', true, 'Transport'),
(24, 'conducteur poids lourd',"3mois", 'Nancy', true, 'Transport'),
(25, 'livreur',"3mois", 'Nancy', true, 'Transport');

-- --------------------------------------------------------
--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email`varchar(255) CHARACTER SET utf8 NOT NULL,
  `nom` varchar(50) CHARACTER SET utf8 NOT NULL,
  `prenom` varchar(50) CHARACTER SET utf8 NOT NULL,
  `mdp` varchar(255) CHARACTER SET utf8
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO `user` (`email`, `nom`, `prenom`) VALUES
('john@gmail.com','`Bou','John');

--
-- Contenu de la table `user`
--

--
-- Index pour les tables exportées
--

--
-- Index pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
