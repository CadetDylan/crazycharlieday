<?php
/**
 * File:  index.php
 * Creation Date: 04/12/2017
 * description:
 *
 * @author: canals
 */

 use \justjob\controleurs\ControleurOffre;
 use \justjob\controleurs\ControleurAccueil;
 use \justjob\controleurs\ControleurUser;
use \justjob\controleurs\ControleurCandidature;
 use \justjob\controleurs\Flash;
 use \justjob\controleurs\Auth;
 use \justjob\controleurs\Utils;

 use Illuminate\Database\Capsule\Manager as DBManager;
 use Psr7Middlewares\Middleware;

require_once __DIR__ . '/vendor/autoload.php';

//Init Eloquent
$db = new DBManager();
$db_conf = parse_ini_file(__DIR__ . "/src/conf/conf.ini");
$db->addConnection($db_conf);
$db->setAsGlobal();
$db->bootEloquent();

//Init Slim
$settings = require_once __DIR__ . "/src/conf/settings.php";
$container = new Slim\Container($settings);
$container["rootDir"] = __DIR__;
$app = new Slim\App($container);

$appMiddlewares = [
    Middleware::TrailingSlash(false) //(optional) set true to add the trailing slash instead remove
        ->redirect(301),
    Flash::class."::flashMiddleware",
    Flash::class."::savePostMiddleware"
];

$requireLogged = Authentification::class."::requireLoggedMiddleware";
$requireAnon = Authentification::class."::requireAnonMiddleware";

foreach($appMiddlewares as $middleware)
{
    $app->add($middleware);
}



//listeCreateur

/*$app->group("/liste", function() use ($app){
    $app->get('/creer', ControleurListeCreateur::class.":afficherFormulaireCreation")->setName("formulaireCreerListe");
    $app->post('/creer', ControleurListeCreateur::class.":creerListe")->setName("creerListe");

    //Routes necessitant une auth
    $app->group("/c{id}", function() use($app){

      

    })->add(ControleurListeCreateur::class."::checkCreateurMiddleware");

});*/

$app->get('/', ControleurAccueil::class.":afficherAccueil")->setName('accueil');


//$app->get('/lien, nomducontrolleur::class."la methode")->setName("le nom pour redirection");

$app->group("/LesOffres", function() use ($app){
    $app->get('', ControleurOffre::class.":afficherOffres")->setName("AffichOffres");
    $app->get('/ajouter', ControleurOffre::class.":afficherAjouterOffre")->setName("afficherFormulaireOffre");
    $app->post('/ajouter',ControleurOffre::class.":ajouterOffre")->setName("ajouterOffre");
	$app->get('/detail/{num}',ControleurOffre::class.":afficherOffreDetail")->setName("afficheOffreDetail");
    $app->get('/detail/{num}/postuler', ControleurOffre::class.":afficherPostuler")->setName("afficherFormulairePostuler");
    $app->post('/detail/{num}/postuler', ControleurOffre::class.":ajouterPostuler")->setName("ajouterPostuler");
    $app->get('/rechercher', ControleurOffre::class.":rechercher")->setName("afficherFormulaireRecherche");
});




$app->get('/covoiturage', ControleurCovoiturage::class.":afficherCovoiturage")->setName("afficherCovoiturage");


/* connection, inscription et deconnexion */
$app->get('/inscription', ControleurUser::class.":afficherFormulaireInscription")->setName("formulaireInscription");
$app->post('/inscription', ControleurUser::class.":inscrire")->setName("inscription");
$app->get('/connexion', ControleurUser::class.":afficherFormulaireConnexion")->setName("formulaireConnexion");
$app->post('/connexion', ControleurUser::class.":connecter")->setName("connexion");
$app->get('/deconnexion', ControleurUser::class.':deconnecter')->setName('deconnexion');


$app->group("/compte", function() use ($app){
    $app->get('', ControleurUser::class.":afficherCompte")->setName("afficherMonCompte");
    $app->get('/mesoffres', ControleurUser::class.":afficherMesOffres")->setName("afficherMesOffres");
    $app->get('/mesoffres/{id}', ControleurUser::class.":afficherMonOffre")->setName("afficherMonOffre");

    $app->get('/choisir/{cand}', ControleurCandidature::class.":choisirCandidature")->setName("choisirCandidature");
});

$app->run();
